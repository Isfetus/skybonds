//
//  ViewFactory.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit

class ViewFactory {
    static func chartComponentController() -> ChartComponentController {
        let mainStoryboard = UIStoryboard(name: Constants.mainStoryoardName, bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: Constants.chartComponentControllerStoryboardId)
        return controller as! ChartComponentController
    }
    static func mainController() -> MainViewController {
        let mainStoryboard = UIStoryboard(name: Constants.mainStoryoardName, bundle: nil)
        let controller = mainStoryboard.instantiateViewController(withIdentifier: Constants.mainControllerStoryoardId)
        return controller as! MainViewController
    }
}
