//
//  Calendar+TimeZone.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 21/05/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import Foundation

extension Calendar {
    static func calendar(GMT:Int) -> Calendar {
        var currentDate = Date()
        currentDate = currentDate.dateWithoutHoursMinutesSeconds()
        var calendar = Calendar.current
        calendar.timeZone =  TimeZone(secondsFromGMT: GMT)!
        return calendar
    }

    static func zeroGTMCalendar() -> Calendar {
        return self.calendar(GMT: 0)
    }
}
