//
//  Date+Utils.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 16/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import Foundation

extension Date {
    
    func create(day: Int, month: Int, year: Int) -> Date {
        var components = DateComponents()
        components.year = year
        components.month = month
        components.day = day

        var calendar = Calendar.current
        calendar.timeZone =  TimeZone(secondsFromGMT: 0)!

        return calendar.date(from: components)!
    }

    public func addDay(calendar: Calendar, count:Int) -> Date {
        var components = DateComponents()
        components.day = count
        return calendar.date(byAdding: components, to: self)!
    }

    public func dateWithoutHoursMinutesSeconds() -> Date {
        var calendar = Calendar.current
        calendar.timeZone =  TimeZone(secondsFromGMT: 0)!

        var components = calendar.dateComponents(in: TimeZone(secondsFromGMT: 0)!, from: self)
        components.second = 0
        components.hour = 0
        components.minute = 0
        return calendar.date(from: components)!
    }

    public func addDaysMonthsYears(calendar: Calendar, days: Int, months: Int, years: Int) -> Date {
        var components = DateComponents()
        components.day = days
        components.month = months
        components.year = years
        return calendar.date(byAdding: components, to: self)!
    }
 }
