//
//  StubBondDataService.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 14/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit

fileprivate let chartMinGeneratedValue: Double = 0.0
fileprivate let chartMaxGeneratedValue: Double = 20.0

class StubBondDataService : StubBondDataServiceProtocol {

    private var response: BondDataWrapper?

    public func getValues(bondDataRequest: BondDataRequest, callback: (BondDataWrapper) -> Void) {
        if self.response == nil {
            self.response = self.generateResponse(bondDataRequest: bondDataRequest)
        }
        callback(self.response!)
    }

    private func generateResponse(bondDataRequest: BondDataRequest) -> BondDataWrapper {
        var currentDate = Date()
        currentDate = currentDate.dateWithoutHoursMinutesSeconds()
        let calendar = Calendar.zeroGTMCalendar()

        var currentPriceValue: Double = 0.0
        var currentYieldValue: Double = 0.0
        var bondValues = [BondValuePlain]()
        var pointerDate = currentDate.addDaysMonthsYears(calendar: calendar, days: 0, months: 0, years: -2)

        while pointerDate.timeIntervalSince1970 <= currentDate.timeIntervalSince1970  {
            currentPriceValue = currentPriceValue + Double.randomValueBetweenMinusOneToOne()
            currentPriceValue = currentPriceValue.round(minLimit: chartMinGeneratedValue, maxLimit: chartMaxGeneratedValue)
            currentYieldValue = currentYieldValue + Double.randomValueBetweenMinusOneToOne()
            currentYieldValue = currentPriceValue.round(minLimit: chartMinGeneratedValue, maxLimit: chartMaxGeneratedValue)
            bondValues.append(BondValuePlain(price: currentPriceValue, yield: currentYieldValue, date: pointerDate))
            pointerDate = pointerDate.addDay(calendar: calendar, count: 1)
        }

        return BondDataWrapper(name: bondDataRequest.bondName, values: bondValues)
    }
}
