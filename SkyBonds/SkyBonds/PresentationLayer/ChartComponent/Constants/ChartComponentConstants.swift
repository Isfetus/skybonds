//
//  ChartComponentConstants.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 14/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

struct ChartComponentConstants {

    static let defaultBondType = "ISIN"
    static let defaultChartState : ChartComponentState = .week

    static let guidelineColor = UIColor.init(red:235.0/255.0, green:235.0/255.0, blue:235.0/255.0, alpha:1.0)
    static let guidelineWidth:CGFloat = 0.3

    static let chartLineColor = UIColor.init(red:229.0/255.0, green:120.0/255.0, blue:132.0/255.0, alpha:1.0)
    static let chartLineWidth:CGFloat = 2.0
    static let chartLineAnimationDuration:Float = 0.0
    static let chartLineAnimationDelay:Float = 0.0

    static let axisLabelFont = UIFont.systemFont(ofSize: 10)
    static let axisLabelRotation:CGFloat = 0.0
    static let axisLineColor = UIColor.init(red:235.0/255.0, green:235.0/255.0, blue:235.0/255.0, alpha:1.0)
    
    static let axisXMinSegmentCount = 2.0
    static let axisXMaxSegmentCount = 10.0
    static let axisXMultiple = 2.0

    static let axisYMinSegmentCount = 2.0
    static let axisYMaxSegmentCount = 10.0
    static let axisYMultiple = 1.0

    static let chartPointLabelOffsetY:CGFloat = -10.0
    static let chartPointLabelShadowOffset = CGSize(width: 0, height: 0)
    static let chartPointLabelShadowOpacity:Float = 1.0
    static let chartPointLabelShadowRadius:CGFloat = 5.0
    static let chartPointLabelFont = UIFont.systemFont(ofSize: 12, weight: .semibold)

    static let periodSwitcherSelectedLabelColor = UIColor.init(red:89.0/255.0, green:135.0/255.0, blue:196.0/255.0, alpha:1.0)
    static let periodSwitcherNormalLabelColor = UIColor.black

    static var iPhoneChartSettings: ChartSettings {
        var chartSettings = ChartSettings()
        chartSettings.leading = 30
        chartSettings.top = 30
        chartSettings.trailing = 30
        chartSettings.bottom = 10
        chartSettings.labelsToAxisSpacingX = 5
        chartSettings.labelsToAxisSpacingY = 5
        chartSettings.axisTitleLabelsToLabelsSpacing = 4
        chartSettings.axisStrokeWidth = 0.3
        chartSettings.spacingBetweenAxesX = 8
        chartSettings.spacingBetweenAxesY = 8
        chartSettings.labelsSpacing = 0
        return chartSettings
    }

    static var iPhoneChartSettingsWithPanZoom: ChartSettings {
        var chartSettings = iPhoneChartSettings
        chartSettings.zoomPan.panEnabled = false
        chartSettings.zoomPan.zoomEnabled = false
        return chartSettings
    }

    static var axisDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM"
        return dateFormatter
    }

    static var readDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter
    }
}
