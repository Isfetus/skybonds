//
//  ChartComponentPresenter.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

class ChartComponentPresenter: ChartComponentViewOutput {

    unowned var view: ChartComponentViewInput
    var service: StubBondDataServiceProtocol
    var bondType: String?
    var state: ChartComponentState = ChartComponentConstants.defaultChartState

    required init(view: ChartComponentViewInput, service: StubBondDataServiceProtocol) {
        self.view = view
        self.service = service
    }

    public func didChangeBondType(bondType: String) {
        self.bondType = bondType
        self.redrawChart(state: self.state)
    }

    public func didChangeState(state: ChartComponentState) {
        self.state = state
        self.redrawChart(state: state)
    }

    private func redrawChart(state: ChartComponentState) {
        self.service.getValues(bondDataRequest: BondDataRequest(bondName: self.bondType!), callback: { [weak self] response in
            let points = ChartPointsMaker.makeChartPoints(bondDataResponse: response, state: state)
            self?.view.drawChart(points: points)
        })
    }
}
