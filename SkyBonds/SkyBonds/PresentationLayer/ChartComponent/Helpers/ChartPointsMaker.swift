//
//  ChartPointsMaker.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 21/05/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

class ChartPointsMaker {

    // make chart points by data and state
    public static func makeChartPoints(bondDataResponse: BondDataWrapper, state: ChartComponentState) -> [ChartPoint] {
        let calendar = Calendar.zeroGTMCalendar()
        let lastDate: Date = bondDataResponse.values.last!.date

        let points : [ChartPoint]
        let startDate: Date
        switch state {
        case .week:
            startDate = lastDate.addDaysMonthsYears(calendar: calendar, days: -7, months: 0, years: 0)
            points = self.makeChartPoints(data: bondDataResponse.values, startDate: startDate)
        case .month:
            startDate = lastDate.addDaysMonthsYears(calendar: calendar, days: 0, months: -1, years: 0)
            points = self.makeChartPoints(data: bondDataResponse.values, startDate: startDate)
        case .threeMonths:
            startDate = lastDate.addDaysMonthsYears(calendar: calendar, days: 0, months: -3, years: 0)
            points = self.makeChartPoints(data: bondDataResponse.values, startDate: startDate)
        case .sixMonths:
            startDate = lastDate.addDaysMonthsYears(calendar: calendar, days: 0, months: -6, years: 0)
            points = self.makeChartPoints(data: bondDataResponse.values, startDate: startDate)
        case .year:
            startDate = lastDate.addDaysMonthsYears(calendar: calendar, days: 0, months: 0, years: -1)
            points = self.makeChartPoints(data: bondDataResponse.values, startDate: startDate)
        case .twoYears:
            startDate = lastDate.addDaysMonthsYears(calendar: calendar, days: 0, months: 0, years: -2)
            points = self.makeChartPoints(data: bondDataResponse.values, startDate: startDate)
        }

        return points;
    }

    // make chart points by data and startDate date
    private static func makeChartPoints(data : [BondValuePlain], startDate: Date) -> [ChartPoint] {
        let readFormatter = ChartComponentConstants.readDateFormatter
        let displayFormatter = ChartComponentConstants.axisDateFormatter

        let filteredVeluesResponses : [BondValuePlain] = data.filter { bondDataResponse -> Bool in
            return bondDataResponse.date.timeIntervalSince1970 >= startDate.timeIntervalSince1970
        }

        let chartPoints = filteredVeluesResponses.map { valuesResponse -> ChartPoint in
            self.createChartPoint(dateStr:readFormatter.string(from: valuesResponse.date), value: valuesResponse.price, readFormatter: readFormatter, displayFormatter: displayFormatter)
        }

        return chartPoints
    }

    private static func createChartPoint(dateStr: String, value: Double, readFormatter: DateFormatter, displayFormatter: DateFormatter) -> ChartPoint {
        return ChartPoint(x: createDateAxisValue(dateStr, readFormatter: readFormatter, displayFormatter: displayFormatter), y: ChartAxisValueDouble(value))
    }

    private static func createDateAxisValue(_ dateStr: String, readFormatter: DateFormatter, displayFormatter: DateFormatter) -> ChartAxisValueDate {
        let date = readFormatter.date(from: dateStr)!
        let labelSettings = ChartLabelSettings(font: ChartComponentConstants.axisLabelFont, rotation: ChartComponentConstants.axisLabelRotation, rotationKeep: .top)
        return ChartAxisValueDate(date: date, formatter: displayFormatter, labelSettings: labelSettings)
    }

}
