//
//  ChartComponentWeekState.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

class ChartComponentWeekState : AbstractChartComponentState {

    override func chartAxisXModel(points: [ChartPoint], labelSettings: ChartLabelSettings) -> ChartAxisModel {
        let xValues = points.map { point -> ChartAxisValue in
            return point.x
        }

        return ChartAxisModel(axisValues: xValues, lineColor: ChartComponentConstants.axisLineColor, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings))
    }

    func createDateAxisValue(_ dateStr: String, readFormatter: DateFormatter, displayFormatter: DateFormatter) -> ChartAxisValue {
        let date = readFormatter.date(from: dateStr)!
        let labelSettings = ChartLabelSettings(font: ChartComponentConstants.axisLabelFont, rotation: ChartComponentConstants.axisLabelRotation, rotationKeep: .top)
        return ChartAxisValueDate(date: date, formatter: displayFormatter, labelSettings: labelSettings)
    }
}
