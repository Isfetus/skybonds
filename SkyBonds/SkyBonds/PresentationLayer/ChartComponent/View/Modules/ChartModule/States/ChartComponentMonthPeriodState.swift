//
//  ChartComponentMonthPeriodState.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 17/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

class ChartComponentMonthPeriodState: AbstractChartComponentState {

    func generatePointView<T, U>(chartPointModel: ChartPointLayerModel<T>, layer: ChartPointsViewsLayer<T,U>, chart: Chart) -> UIView? {
        return nil
    }

    override func chartAxisXModel(points: [ChartPoint], labelSettings: ChartLabelSettings) -> ChartAxisModel {
        let displayFormatter = ChartComponentConstants.axisDateFormatter
        let xValues = ChartAxisValuesStaticGenerator.generateXAxisValuesWithChartPoints(points,
                                                                                        minSegmentCount: ChartComponentConstants.axisXMinSegmentCount,
                                                                                        maxSegmentCount: ChartComponentConstants.axisXMaxSegmentCount,
                                                                                        multiple: ChartComponentConstants.axisXMultiple,
                                                                                        axisValueGenerator: {  ChartAxisValueDate(date: ChartAxisValueDate.dateFromScalar($0),
                                                                                                                                  formatter: displayFormatter,
                                                                                                                                  labelSettings: labelSettings)},
                                                                                        addPaddingSegmentIfEdge: false)
        return ChartAxisModel(axisValues: xValues, lineColor: ChartComponentConstants.axisLineColor, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings))
    }

    func createDateAxisValue(_ dateStr: String, readFormatter: DateFormatter, displayFormatter: DateFormatter) -> ChartAxisValue {
        let date = readFormatter.date(from: dateStr)!
        let labelSettings = ChartLabelSettings(font: ChartComponentConstants.axisLabelFont, rotation: ChartComponentConstants.axisLabelRotation, rotationKeep: .top)
        return ChartAxisValueDate(date: date, formatter: displayFormatter, labelSettings: labelSettings)
    }

    override func chartPointLabel<T, U>(chartPointModel: ChartPointLayerModel<T>, layer: ChartPointsViewsLayer<T,U>, chart: Chart) -> UILabel? {
        return nil
    }
}
