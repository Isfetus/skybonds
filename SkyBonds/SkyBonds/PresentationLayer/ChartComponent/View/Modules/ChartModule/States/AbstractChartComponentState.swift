//
//  AbstractChartComponentState.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

class AbstractChartComponentState {

    unowned let viewController: ChartComponentController

    init(viewController: ChartComponentController) {
        self.viewController = viewController;
    }

    func drawChart(points: [ChartPoint]) {
        //settings
        let labelSettings = self.chartLabelSettings()
        let chartSettings = self.chartSettings()
        let chartFrame = self.viewController.chartContainer.frame

        // coords space
        let xModel = self.chartAxisXModel(points: points, labelSettings: labelSettings)
        let yModel = self.chartAxisYModel(points: points, labelSettings: labelSettings)
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)

        // line
        let (xAxisLayer, yAxisLayer) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer)
        let lineModel = self.chartLineModel(points: points)
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, lineModels: [lineModel])

        // points labels
        let viewGenerator = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsViewsLayer, chart: Chart) -> UIView? in
            return self.chartPointLabel(chartPointModel: chartPointModel, layer: layer, chart: chart)
        }
        let chartPointsLayer = ChartPointsViewsLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: points, viewGenerator: viewGenerator, mode: .translate)

        // guidelinew
        let settings = ChartGuideLinesLayerSettings.init(linesColor: ChartComponentConstants.guidelineColor, linesWidth: ChartComponentConstants.guidelineWidth)
        let guidelinesLayer = ChartGuideLinesLayer.init(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)

        let chart = self.chart(frame: chartFrame, innerFrame: nil, settings: chartSettings, layers: [
            xAxisLayer,
            yAxisLayer,
            guidelinesLayer,
            chartPointsLineLayer,
            chartPointsLayer
            ])

        self.viewController.chartContainer.addSubview(chart.view)
        self.viewController.chart = chart
    }

    // MARK: Settings
    func chartLabelSettings() -> ChartLabelSettings {
        return ChartLabelSettings(font: ChartComponentConstants.axisLabelFont)
    }

    func chartAxisXModel(points: [ChartPoint], labelSettings: ChartLabelSettings) -> ChartAxisModel {
        fatalError("chartAxisXModel has not been implemented")
    }

    func chartAxisYModel(points: [ChartPoint], labelSettings: ChartLabelSettings) -> ChartAxisModel {
        let yValues = ChartAxisValuesStaticGenerator.generateYAxisValuesWithChartPoints(points,
                                                                                        minSegmentCount: ChartComponentConstants.axisYMinSegmentCount,
                                                                                        maxSegmentCount: ChartComponentConstants.axisYMaxSegmentCount,
                                                                                        multiple: ChartComponentConstants.axisYMultiple,
                                                                                        axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)},
                                                                                        addPaddingSegmentIfEdge: false)
        return ChartAxisModel(axisValues: yValues, lineColor: ChartComponentConstants.axisLineColor, axisTitleLabel: ChartAxisLabel(text: "", settings: labelSettings))
    }

    func chartSettings() -> ChartSettings {
        return ChartComponentConstants.iPhoneChartSettingsWithPanZoom
    }

    func chartLineModel<T: ChartPoint>(points: [T]) -> ChartLineModel<T> {
        return ChartLineModel<T>(chartPoints:points,
                                 lineColor:ChartComponentConstants.chartLineColor,
                                 lineWidth:ChartComponentConstants.chartLineWidth,
                                 lineJoin:.round,
                                 lineCap:.round,
                                 animDuration:ChartComponentConstants.chartLineAnimationDuration,
                                 animDelay:ChartComponentConstants.chartLineAnimationDelay)
    }

    func chartPointLabel<T, U>(chartPointModel: ChartPointLayerModel<T>, layer: ChartPointsViewsLayer<T,U>, chart: Chart) -> UILabel? {
        let label = UILabel()
        label.text = chartPointModel.chartPoint.y.description
        label.font = ChartComponentConstants.chartPointLabelFont
        label.textAlignment = NSTextAlignment.center
        label.shadowColor = .white
        label.layer.shadowColor = UIColor.white.cgColor
        label.layer.shadowOffset = ChartComponentConstants.chartPointLabelShadowOffset
        label.layer.shadowOpacity = ChartComponentConstants.chartPointLabelShadowOpacity
        label.layer.shadowRadius = ChartComponentConstants.chartPointLabelShadowRadius
        label.sizeToFit()

        let center = CGPoint(x: chartPointModel.screenLoc.x , y: chartPointModel.screenLoc.y + ChartComponentConstants.chartPointLabelOffsetY )
        label.center = center;
        return label;
    }

    func chart(frame: CGRect, innerFrame: CGRect? = nil, settings: ChartSettings, layers: [ChartLayer]) -> Chart {
        return Chart(
            frame: frame,
            innerFrame: innerFrame,
            settings: settings,
            layers: layers)
    }
}
