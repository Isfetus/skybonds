//
//  ChartComponentState.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

enum ChartComponentState: String {
    case week = "1W"
    case month = "1M"
    case threeMonths = "3M"
    case sixMonths = "6M"
    case year = "1Y"
    case twoYears = "2Y"

    static let allValues = [week, month, threeMonths, sixMonths, year, twoYears]
}

