//
//  ChartComponentController.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 14/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit
import SwiftCharts

class ChartComponentController: UIViewController, ChartComponentViewInput {

    public var chart: Chart?
    public var presenter:ChartComponentViewOutput?
    public var state:AbstractChartComponentState!

    @IBOutlet var chartContainer: UIView!
    @IBOutlet var stateSegmentedControl: UISegmentedControl!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.state = ChartComponentWeekState(viewController: self)
    }

    // MARK : Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.drawChartIfNeeded()
    }

    func drawChartIfNeeded() {
        guard self.chart == nil else {
            return
        }
        self.presenter?.didChangeState(state: ChartComponentConstants.defaultChartState)
    }

    // MARK : Layout
    func setupLayout() {
        self.setStyleForSegmentedControl()
        self.configurateStateSegmentedControl()
    }

    func setStyleForSegmentedControl() {
        self.stateSegmentedControl.tintColor = .clear
        let selectedLabelTextAttributes = [NSAttributedStringKey.foregroundColor: ChartComponentConstants.periodSwitcherSelectedLabelColor]
        self.stateSegmentedControl.setTitleTextAttributes(selectedLabelTextAttributes, for: .selected)
        let defaultLabelTextAttributes = [NSAttributedStringKey.foregroundColor: ChartComponentConstants.periodSwitcherNormalLabelColor]
        self.stateSegmentedControl   .setTitleTextAttributes(defaultLabelTextAttributes, for: .normal)
    }

    func configurateStateSegmentedControl() {
        self.stateSegmentedControl.removeAllSegments()
        var i: Int = 0
        for state in ChartComponentState.allValues {
            self.stateSegmentedControl.insertSegment(withTitle: state.rawValue, at: i, animated: false)
            i = i + 1
        }
        self.stateSegmentedControl.selectedSegmentIndex = ChartComponentState.allValues.index(of: ChartComponentConstants.defaultChartState)!;
    }

    // MARK : ChartComponentViewInput
    func drawChart(points: [ChartPoint]) {
        if self.chart != nil {
            self.chart!.view.removeFromSuperview()
            self.chart = nil
        }
        self.state.drawChart(points: points)
    }

    // MARK : State changing	            
    public func setState(state: ChartComponentState) {
        switch state {
        case .week:
            self.state = ChartComponentWeekState(viewController: self)
        case .month:
            self.state = ChartComponentMonthPeriodState(viewController: self)
        case .threeMonths:
            self.state = ChartComponentMonthsPeriodState(viewController: self)
        case .sixMonths:
            self.state = ChartComponentMonthsPeriodState(viewController: self)
        case .year:
            self.state = ChartComponentYearPeriodState(viewController: self)
        case .twoYears:
            self.state = ChartComponentYearPeriodState(viewController: self)
        }

        self.presenter?.didChangeState(state: state)
    }

    // MARK : Change state segmented control action
    @IBAction func onStateChanged(sender: UISegmentedControl) {
        let state = ChartComponentState.allValues[sender.selectedSegmentIndex]
        self.setState(state: state)
    }
}
