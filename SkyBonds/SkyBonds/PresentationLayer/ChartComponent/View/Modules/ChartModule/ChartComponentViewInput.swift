//
//  ChartComponentViewInput.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 15/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import Foundation
import SwiftCharts

protocol ChartComponentViewInput : class {
    func drawChart(points: [ChartPoint])
}
