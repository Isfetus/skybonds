//
//  MainViewController.swift
//  SkyBonds
//
//  Created by Andrey Belkin on 14/04/2018.
//  Copyright © 2018 Andrew Belkin. All rights reserved.
//

import UIKit


class MainViewController: UIViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MainViewConstants.chartComponentEmbedSegueIdentifier {
            let chartComponentController = segue.destination as! ChartComponentController
            chartComponentController.presenter = PresenterFactory.chartComponentPresenter(view: chartComponentController, bondType: ChartComponentConstants.defaultBondType)
        }
    }
}

